const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Delete consecutive duplicates in a list', () => {
    context('When list is not passed', () => {
        it('shouls return nothing', () => {
            const result = app.deleteConsecutiveDuplicates()
            expect(result).to.be.undefined
        });
    });
    context('When an empty list passed', () => {
        it('should return empty lisr', () => {
            const result = app.deleteConsecutiveDuplicates([])
            expect(result).to.be.eql([])
        });
    });
    context('When list without consecutive duplicates is passed', () => {
        it('should return the same list', () => {
            const result = app.deleteConsecutiveDuplicates([1, 2])
            expect(result).to.be.eql([1, 2])
            const result2 = app.deleteConsecutiveDuplicates('ab')
            expect(result2).to.be.eql('ab')
        });
    });
    context('when list with consecutive duplicates is passed', () => {
        it('should remove the duplicates and return the list', () => {
            const result = app.deleteConsecutiveDuplicates([1, 1, 2])
            expect(result).to.be.eql([1, 2])
            const result2 = app.deleteConsecutiveDuplicates('abb')
            expect(result2).to.be.eql('ab')
        });
    });
});