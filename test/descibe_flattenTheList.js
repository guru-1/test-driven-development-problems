const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Flatten the list', () => {
    context('when list is not passed', () => {
        it('should return nothing', () => {
            const result = app.flattenTheList()
            expect(result).to.be.undefined
        });
    });
    context('When an empty list is passed', () => {
        it('should return empty list', () => {
            const result = app.flattenTheList([])
            expect(result).to.be.eql([])
        });
    });
    context('when a flatten list is passed', () => {
        it('should return the same list', () => {
            const result = app.flattenTheList([1, 2])
            expect(result).to.be.eql([1, 2])
        });
    });
    context('when a nested list is passed', () => {
        it('should return the flatted version of the list', () => {
            const result = app.flattenTheList([1, 2, [1, 2]])
            expect(result).to.be.eql([1, 2, 1, 2])
        });
    });
});