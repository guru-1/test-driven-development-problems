const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Reverse of a list', () => {
    context('when list is not passed', () => {
        it('should return nothing', () => {
            const result = app.reverseOfList()
            expect(result).to.be.undefined
        });
    });
    context('when an empty list is passed', () => {
        it('should return empty list', () => {
            const result = app.reverseOfList([])
            expect(result).to.be.eql([])
        });
    });
    context('when list with single element is passed', () => {
        it('should return the same list', () => {
            const result = app.reverseOfList('a')
            expect(result).to.be.eql('a')
        });
    });
    context('when list with more than single element is passed', () => {
        it('should return reverse of the list', () => {
            const result = app.reverseOfList([1, 2])
            expect(result).to.be.eql([2, 1])
            const result2 = app.reverseOfList('abcd')
            expect(result2).to.be.eql('dcba')
        });
    });
});