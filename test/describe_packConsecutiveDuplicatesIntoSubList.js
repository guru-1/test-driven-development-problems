const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Pack consecutive duplicates into sublist', () => {
    context('when list is not passed', () => {
        it('should return nothing', () => {
            const result = app.packConsecutiveDuplicates()
            expect(result).to.be.undefined
        });
    });
    context('when an empty list is passed', () => {
        it('should return empty list', () => {
            const result = app.packConsecutiveDuplicates([])
            expect(result).to.be.eql([])
        });
    });
    context('When list without consecutive duplicates is passed', () => {
        it('should return the same list', () => {
            const result = app.packConsecutiveDuplicates([1, 2])
            expect(result).to.be.eql([1, 2])
            const result2 = app.packConsecutiveDuplicates(['a', 'b'])
            expect(result2).to.be.eql(['a', 'b'])
        });
    });
    context('when list with consecutive duplicates is passed', () => {
        it('should pack the consecutive elements into sublists and return the list', () => {
            const result = app.packConsecutiveDuplicates([1, 1, 1, 2])
            expect(result).to.be.eql([[1, 1, 1], 2])
            const result2 = app.packConsecutiveDuplicates(['a', 'b', 'b'])
            expect(result2).to.be.eql(['a', 'bb'])
        });
    });
});