const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('find nth Element of list ', () => {
    context('when list and position is not passed', () => {
        it('should return nothing', () => {
            const result = app.findNthElementOfList()
            expect(result).to.be.undefined
        });
    });
    context('when position is not passed', () => {
        it('should return nothing', () => {
            const result = app.findNthElementOfList([1, 2])
            expect(result).to.be.undefined
        });
    });
    context('when datatype of position sent is wrong ', () => {
        it('should return nothing', () => {
            const result = app.findNthElementOfList([1, 2], 'z')
            expect(result).to.be.undefined
        });
    });
    context('when position is beyond length of the list', () => {
        it('should return nothing', () => {
            const result = app.findNthElementOfList([1, 2, 3], 4)
            expect(result).to.be.undefined
        });
    });
    context('when position is within the length of the list', () => {
        it('should return value at requested position', () => {
            const result = app.findNthElementOfList([1, 18], 2)
            expect(result).to.be.eql(18)
        });
    })
});