const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Find the last element in the list ', () => {
    context('When list is not passed', () => {
        it('should return nothing', () => {
            const result = app.findLastElement()
            expect(result).to.be.undefined
        });
    });
    context('when list is empty', () => {
        it('should return nothing', () => {
            const result = app.findLastElement()
            expect(result).to.be.undefined
        });
    });
    context('When list contains only one element', () => {
        it('should return first element', () => {
            const result = app.findLastElement([18])
            expect(result).to.be.eql(18)
        });
    });
    context('When list contains more than one element', () => {
        it('should return last element', () => {
            const result = app.findLastElement([18, 45, 7, 10])
            expect(result).to.be.eql(10)
        });
    });
});

