const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Find the length of the consecutive duplicate elements in a list', () => {
    context('when list is not passed', () => {
        it('should return nothing', () => {
            const result = app.lengthOfConsecutiveDuplicateElements()
            expect(result).to.be.undefined
        });
    });
    context('when an empty list is passed', () => {
        it('should return empty list', () => {
            const result = app.lengthOfConsecutiveDuplicateElements([])
            expect(result).to.be.eql([])
        });
    });
    context('when list without any consecutive duplicates are passed', () => {
        it('should return count as 1 for every element in sublist', () => {
            const result = app.lengthOfConsecutiveDuplicateElements([1, 2, 1])
            expect(result).to.be.eql([[1, 1], [1, 2], [1, 1]])
        });
    });
    context('when list without any consecutive duplicates are passed', () => {
        it('should return count as number of occurence for every element in sublist', () => {
            const result = app.lengthOfConsecutiveDuplicateElements([1, 1, 2, 3, 3, 4, 4, 4, 1])
            expect(result).to.be.eql([[2, 1], [1, 2], [2, 3], [3, 4], [1, 1]])
        });
    });
});