const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Find whether the list is pallindrome or not', () => {
    context('when list is not passed to the function', () => {
        it('should return nothing', () => {
            const result = app.pallindrome()
            expect(result).to.be.undefined
        });
    });
    context('When an empty list is passed', () => {
        it('should return true', () => {
            const result = app.pallindrome([])
            expect(result).to.be.eql(true)
        });
    });
    context('when a list which is not equal to reverse of it is passed (Pallindrome condition)', () => {
        it('should return false', () => {
            const result = app.pallindrome([1, 2, 3])
            expect(result).to.be.eql(false)
            const result2 = app.pallindrome('abc')
            expect(result2).to.be.eql(false)
        });
    });
    context('When a list which is equal to reverse of it is passed (Pallindrome consition)', () => {
        it('should return true', () => {
            const result = app.pallindrome([1, 2, 1])
            expect(result).to.be.eql(true)
            const result2 = app.pallindrome('abccba')
            expect(result2).to.be.eql(true)
        });
    });
});