const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Find last But one element in the list', () => {
    context('When list is empty', () => {
        it('should return nothing', () => {
            const result = app.findLastButOneElement()
            expect(result).to.be.undefined
        });
    });
    context('When list has only one element', () => {
        it('should return nothing', () => {
            const result = app.findLastButOneElement([56])
            expect(result).to.be.undefined
        });
    });
    context('When list has only two elements', () => {
        it('should return first element', () => {
            const result = app.findLastButOneElement([1, 2])
            expect(result).to.be.eql(1)
        });
    });
    context('When list has more than two elements', () => {
        it('should return last but one element', () => {
            const result = app.findLastButOneElement([1, 2, 'guru', 1])
            expect(result).to.be.eql('guru')
        });
    });
});
