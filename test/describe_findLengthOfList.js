const chai = require('chai')
const expect = chai.expect
const app = require('../app')

describe('Find the length of the list ', () => {
    context('when nothing is passed', () => {
        it('should return nothing', () => {
            const result = app.findLengthOfList()
            expect(result).to.be.undefined
        });
    });
    context('when datatype of list sent is wrong', () => {
        it('should return nothing', () => {
            const result = app.findLengthOfList({})
            expect(result).to.be.undefined
        });
    });
    context('when list is sent correctly', () => {
        it('should return the length of the list', () => {
            const result = app.findLengthOfList([1, 2])
            expect(result).to.be.eql(2)
            const result2 = app.findLengthOfList("ab")
            expect(result2).to.be.eql(2)
        });
    });
});