module.exports = {
    findLastElement: (list = []) => {
        if (!Array.isArray(list) && typeof list !== 'string') return
        return list[list.length - 1]
    },
    findLastButOneElement: (list = []) => {
        if (!Array.isArray(list) && typeof list !== 'string') return
        return list[list.length - 2]
    },
    findNthElementOfList: (list, position) => {
        if (!list || !position || (!Array.isArray(list) && typeof list !== 'string')) return
        return list[position - 1]
    },
    findLengthOfList: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        return list.length
    },
    reverseOfList: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        if (typeof list === 'string') return list.split('').reverse().join('')
        return list.reverse()
    },
    pallindrome: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        if (typeof list === 'string') return list === list.split('').reverse().join('')
        return list.every((ele, index) => ele === list.reverse()[index]);
    },
    flattenTheList: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        const res = []
        function flatten(list) {
            list.forEach(ele => {
                if (!Array.isArray(ele)) res.push(ele)
                else flatten(ele)
            })
        }
        flatten(list)
        return res
    },
    deleteConsecutiveDuplicates: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        let isString = false
        if (typeof list === 'string') {
            isString = true
            list = list.split('')
        }
        const res = []
        list.forEach((ele, index) => {
            if (ele !== list[index - 1]) res.push(ele)
        })
        if (!isString) return res
        return res.join('')
    },
    packConsecutiveDuplicates: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        const res = []
        for (let i = 0; i < list.length; i++) {
            let sublist
            let isString = false
            if (typeof list[i] === 'string') {
                isString = true
                sublist = list[i]
            } else {
                list[i] === list[i + 1] ? sublist = [list[i]] : sublist = list[i]
            }
            for (j = i + 1; j < list.length; j++) {
                if (list[i] !== list[j]) {
                    break
                }
                if (isString) sublist += list[j]
                else sublist.push(list[i])
            }
            i = j - 1
            res.push(sublist)
        }
        return res
    },
    lengthOfConsecutiveDuplicateElements: (list) => {
        if (!list || (!Array.isArray(list) && typeof list !== 'string')) return
        const res = []
        let count = 1
        for (i = 0; i < list.length; i++) {
            if (list[i] !== list[i + 1]) {
                res.push([count, list[i]])
                count = 1
            }
            else count += 1
        }
        return res
    }
}
